//
//  ViewController.m
//  ScreenDesign
//
//  Created by Click Labs134 on 10/8/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"
UIGestureRecognizer *tapper;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *personalInfoLabel;
@property (strong, nonatomic) IBOutlet UITextField *firstNameTextFiels;
@property (strong, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UIImageView *phoneNumberTextField;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;


@end

@implementation ViewController

@synthesize personalInfoLabel;
@synthesize firstNameTextFiels;
@synthesize lastNameTextField;
@synthesize emailTextField;
@synthesize phoneNumberTextField;
@synthesize profileImage;
@synthesize phoneTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //for circle image
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.clipsToBounds = YES;
    
    //textfiled color
     firstNameTextFiels.layer.borderColor = [[UIColor blueColor] CGColor];
    firstNameTextFiels.layer.borderWidth = 1.0f;
    
    lastNameTextField.layer.borderColor = [[UIColor blueColor] CGColor];
    lastNameTextField.layer.borderWidth = 1.0f;
    
    emailTextField.layer.borderColor = [[UIColor blueColor] CGColor];
    emailTextField.layer.borderWidth = 1.0f;
    
    phoneTextField.layer.borderColor = [[UIColor blueColor] CGColor];
    phoneTextField.layer.borderWidth = 1.0f;
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];

    
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

-(BOOL)checkEmailID:(NSString*)text{
    NSCharacterSet *c=[[NSCharacterSet characterSetWithCharactersInString:@"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789.@"]invertedSet];
    if ([text rangeOfCharacterFromSet:c].location==NSNotFound) {
        NSLog(@"No Speacial Character");
    } else {
        NSLog(@"Speacial Character Found");
        return YES;
    }
    return NO;
}

-(BOOL)checkPhoneNumber:(NSString*)text{
    NSCharacterSet *c=[[NSCharacterSet characterSetWithCharactersInString:@"1234567890+-"]invertedSet];
    if ([text rangeOfCharacterFromSet:c].location==NSNotFound) {
        NSLog(@"No Speacial Character");
    } else {
        NSLog(@"Speacial Character Found");
        return YES;
    }
    return NO;
}

-(BOOL)checkName:(NSString*)text{
    NSCharacterSet *c=[[NSCharacterSet characterSetWithCharactersInString:@"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"]invertedSet];
    if ([text rangeOfCharacterFromSet:c].location==NSNotFound) {
        NSLog(@"No Speacial Character");
    } else {
        NSLog(@"Speacial Character Found");
        return YES;
    }
    return NO;
}

- (IBAction)doneButton:(id)sender {
        if ([self checkEmailID: emailTextField.text] == YES){
            
            [self alert1];
            
        }
        
        else if ([self checkPhoneNumber: phoneTextField.text] == YES){
            
            [self alert2];
        }
        else if ([self checkName: firstNameTextFiels.text] == YES){
            
            [self alert3];
        }
        else if ([self checkName: lastNameTextField.text] == YES){
            
            [self alert4];
        }
        else
        {
            UIAlertView *done = [[UIAlertView alloc]initWithTitle:@"Registered" message:@"Profile Edited" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
            [done show];
        }
}

-(void)alert1{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"EMail-ID" message:@"Enter Valid Email ID" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert2{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Phone Number" message:@"Enter valid phone number" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert3{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"First Name" message:@"Enter valid name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert4{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"Last Name" message:@"Enter valid name" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
